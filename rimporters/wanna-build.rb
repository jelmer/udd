#!/usr/bin/ruby

def update_wanna_build
  db = PG.connect(UDD_USER_PG)
  db.exec("BEGIN")
  db.exec("SET CONSTRAINTS ALL DEFERRED")
  db.exec("DELETE FROM wannabuild")

  db.prepare('wb_insert', "INSERT INTO wannabuild
      (architecture, source, distribution, version, state, installed_version, previous_state, state_change, binary_nmu_version, notes, vancouvered) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)")

  wb = PG.connect("service=wanna-build")
  archs = wb.exec("SELECT distribution, architecture, vancouvered FROM distribution_architectures")
  archs.each do |res|
    wb.exec("SELECT package, distribution, version, state, installed_version, previous_state, state_change, binary_nmu_version, notes FROM \"#{res['architecture']}_public\".packages where distribution=\'#{res['distribution']}\'").each do |r|
      db.exec("SAVEPOINT mysavepoint")
      begin
        db.exec_prepared('wb_insert', [ res['architecture'], r['package'], r['distribution'], r['version'], r['state'], r['installed_version'], r['previous_state'], r['state_change'], r['binary_nmu_version'], r['notes'], res['vancouvered']])
        db.exec("RELEASE SAVEPOINT mysavepoint")
      rescue Exception => e
        puts e.message
        db.exec("ROLLBACK TO SAVEPOINT mysavepoint")
      end
    end
  end
  db.exec("COMMIT")
end
