#!/usr/bin/ruby

$:.unshift('../../rlibs')
require 'udd-db'
require 'pp'
require 'json'
require 'fileutils'

puts "Content-type: text/plain\n\n"

def get_pas
  td = `mktemp -d`.chomp
  system("cd #{td} && git clone --no-checkout https://buildd.debian.org/git/packages-arch-specific.git > /dev/null 2>&1") or raise
  pas = `cd #{td}/packages-arch-specific && git show HEAD:Packages-arch-specific`
  raise "error" if $? != 0
  FileUtils::rm_rf(td)
  return pas
end

# calculate $archs
$archs = {}
pas = get_pas
arr = pas.split(/\n/).grep(/^%?[a-z0-9]/).map { |l| l.gsub(/\s*#.*$/,'') }
allarchs=['amd64','i386']
arr.each do |l|
  pkg, archsl = l.split(/:\s*/, 2)
  if archsl.nil?
     STDERR.puts "NIL: #{l}"
     next
  end
  #next if pkg !~ /^%/ # we ignore binary packages
  pkg = pkg.gsub(/^%/,'')
  archs = archsl.split(' ')
  if archsl =~ /!/
    # remove mode
    march = allarchs.clone
    archs.each do |a|
      march = march - [a.gsub(/^!/,'')]
    end
  else
    march = archs
  end
  $archs[pkg] = march
end

# key packages
DB = Sequel.connect(UDD_GUEST)
kp = DB["select source from key_packages"].all.hash_values.flatten


sources = DB["
select
	source,
	version,
	release,
	architecture,
	component,
	extra_source_only
from
	sources_uniq
where
		distribution='debian'
	and release in (
		select
			release
		from
			releases
		where
			role in ('testing','unstable')
	)
	and (architecture ~ 'all' or architecture ~ 'any' or architecture ~ 'amd64' or architecture ~ 'i386')
order by
	source,
	version
	"].all.sym2str

# fetch RC bugs status
sources_rc_bug_testing = DB["select distinct bugs.source from bugs_rt_affects_testing inner join bugs on bugs.id = bugs_rt_affects_testing.id where bugs.severity >= 'serious'"].all.hash_values.flatten
sources_rc_bug_unstable = DB["select distinct bugs.source from bugs_rt_affects_unstable inner join bugs on bugs.id = bugs_rt_affects_unstable.id where bugs.severity >= 'serious'"].all.hash_values.flatten
sources_ftbfs_bug_testing = DB["select distinct bugs.source from bugs_rt_affects_testing inner join bugs on bugs.id = bugs_rt_affects_testing.id where bugs.severity >= 'serious' and bugs.title ~ 'FTBFS'"].all.hash_values.flatten
sources_ftbfs_bug_unstable = DB["select distinct bugs.source from bugs_rt_affects_unstable inner join bugs on bugs.id = bugs_rt_affects_unstable.id where bugs.severity >= 'serious' and bugs.title ~ 'FTBFS'"].all.hash_values.flatten

h = {}
sources.each do |r|
  if not h.has_key?(r['source'])
    h[r['source']] = {}
    h[r['source']]['key'] = kp.include?(r['source'])
    h[r['source']]['pas'] = $archs[r['source']]
  end
  h[r['source']][r['release']] = { 'version' => r['version'], 'component' => r['component'], 'architecture' => r['architecture'].split(' '), 'extra_source_only' => r['extra_source_only'] || false }
  h[r['source']]['rc-bug-testing'] = sources_rc_bug_testing.include?(r['source'])
  h[r['source']]['rc-bug-unstable'] = sources_rc_bug_unstable.include?(r['source'])
  h[r['source']]['ftbfs-bug-testing'] = sources_ftbfs_bug_testing.include?(r['source'])
  h[r['source']]['ftbfs-bug-unstable'] = sources_ftbfs_bug_unstable.include?(r['source'])
end

#h.each_pair do |source, v|
#  h[source]['build'] = []
#  h[source]['build'] << 'amd64' if (v['sid']['architecture'] & ['any', 'amd64']) != [] 
#end

puts JSON::pretty_generate(h)
