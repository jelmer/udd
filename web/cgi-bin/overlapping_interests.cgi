#!/usr/bin/python3
# -*- coding: utf-8; mode: python; tab-width: 4; -*-

"""
Takes two arguments deva and devb which are the maintainers emails
and returns a list of package names. This list consists of the packages
of devb that are a dependency to some package maintained by deva.
"""

import sys
import os
sys.path.insert(0, os.path.abspath('../../pylibs/'))
from cgi_helpers import *
import cgi
import re

import psycopg2


DATABASE = 'service=udd'


def execute_query(query):
    conn = psycopg2.connect(DATABASE)
    cursor = conn.cursor()
    cursor.execute(query)

    row = cursor.fetchone()
    while not row is None:
        yield row
        row = cursor.fetchone()

    cursor.close()
    conn.close()

QUERY_MAINTAINER_DEPENDS = """\
SELECT DISTINCT depends
  FROM all_packages
 WHERE maintainer_email='{0}'
       AND depends is not NULL
"""
def qet_maintainer_depends(maintainer):
    query = QUERY_MAINTAINER_DEPENDS.format(maintainer)
    depends = set()
    for row in execute_query(query):
        dep_line = row[0]
        dep_line = dep_line.replace(',', ' ').replace('|', ' ')
        # Remove versions from versioned depends
        dep_line = re.sub('\(.*\)', '', dep_line)

        for x in dep_line.split(' '):
            stripped = x.strip()

            if stripped :
                #add ' character so they can be imported
                #into a where clause of another query
                depends.add("'"+stripped+"'")

    return list(depends)

QUERY_OVERLAPPING_PKGS = """\
SELECT DISTINCT package
  FROM all_packages
 WHERE package in ({0})
       AND maintainer_email='{1}'
"""
def get_overlapping_pks(depends, maintainer):
    query = QUERY_OVERLAPPING_PKGS.format(','.join(depends), maintainer)
    for row in execute_query(query):
        yield row[0]

def main():
    print_contenttype_header('text/html')

    arguments = cgi.FieldStorage()

    if not "deva" in arguments or not "devb" in arguments:
        print("Not deva or devb argument was provided.")
        sys.exit(-1)

    depends = qet_maintainer_depends(arguments["deva"].value)
    pkgs = get_overlapping_pks(depends, arguments["devb"].value)

    for pkg in pkgs:
        print(pkg)

if __name__ == '__main__':
    main()
